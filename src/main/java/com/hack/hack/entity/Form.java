package com.hack.hack.entity;

import javax.persistence.*;

@Entity
public class Form {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;


    private String formName;


    private String path;

    @ManyToOne(targetEntity = Compliance.class)
    private Compliance compliance;
    
    @OneToOne(targetEntity= FormTemplate.class)
    private FormTemplate formTemplate;
    
    

    public FormTemplate getFormTemplate() {
		return formTemplate;
	}

	public void setFormTemplate(FormTemplate formTemplate) {
		this.formTemplate = formTemplate;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Compliance getCompliance() {
        return compliance;
    }

    public void setCompliance(Compliance compliance) {
        this.compliance = compliance;
    }
}
