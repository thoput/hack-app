package com.hack.hack.constant;

public enum Status {
    NEW(1),
    IN_PROGRESS(2),
    COMPLETED(3);

    private int status;
    Status(int i) {
        this.status = i;
    }
    public int getStatus () {
        return  this.status;
    }
}
