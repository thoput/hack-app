package com.hack.hack.service;

import com.hack.hack.entity.Notification;
import com.hack.hack.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    public void save(Notification notification) {
        notificationRepository.save(notification);
    }

    public List<Notification> getNotifications() {
        return (List<Notification>) notificationRepository.findAll();
    }
}
