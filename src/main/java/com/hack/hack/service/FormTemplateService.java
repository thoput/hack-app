package com.hack.hack.service;

import com.hack.hack.entity.ComplianceTemplate;
import com.hack.hack.entity.FormTemplate;
import com.hack.hack.repository.ComplianceTemplateRepository;
import com.hack.hack.repository.FormTemplateRepository;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class FormTemplateService {

    @Autowired
    private FormTemplateRepository formTemplateRepository;

    @Autowired
    private ComplianceTemplateRepository complianceTemplateRepository;

    public FormTemplate saveName(String formName, Integer complianceId) {
        final Optional<ComplianceTemplate> byId = complianceTemplateRepository.findById(complianceId);
        byId.orElseThrow(() -> new ServiceException("Compliance Template Not Present"));
        FormTemplate formTemplate = new FormTemplate();
        formTemplate.setFormName(formName);
        formTemplate.setCreatedDate(new Date());
        formTemplate.setUpdatedDate(new Date());
        formTemplate.setComplianceTemplate(byId.get());
        return formTemplateRepository.save(formTemplate);
    };

    public FormTemplate savePath(Integer formId, String path) {
        final Optional<FormTemplate> byId = formTemplateRepository.findById(formId);
        byId.orElseThrow(() -> new ServiceException("Form does not exist"));
        FormTemplate formTemplate = byId.get();
        formTemplate.setPath(path);
        return formTemplateRepository.save(formTemplate);
    }

    public String delete(Integer id) {
        Optional<FormTemplate> formTemplate = formTemplateRepository.findById(id);
        formTemplate.orElseThrow(() -> new ServiceException("Form Template Not Present"));
        formTemplateRepository.delete(formTemplate.get());
        return formTemplate.get().getPath();
    }
}
