package com.hack.hack.service;

import com.hack.hack.entity.SearchResultItem;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.util.UriEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchService {
    public List<SearchResultItem> search(String query) {
        List<SearchResultItem> resultItems = new ArrayList<>();
        String[] urls = new String[1];
        urls[0] = "https://www.bing.com/search?q=" + UriEncoder.encode(query) + "&format=rss";
//        urls[0] = "https://news.google.com/rss/search?q=" + UriEncoder.encode(query);
        for (int i = 0; i < urls.length; i++) {
            getRSSFeeds(resultItems, urls[i]);
        }
        return resultItems;
    }

    private void getRSSFeeds(List<SearchResultItem> resultItems, String url) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create()
                    .build();
            HttpGet get = new HttpGet(url);
            HttpResponse response = client.execute(get);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputStream stream = response.getEntity()
                    .getContent();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder sb = new StringBuilder();
            String inline = "";
            while ((inline = inputReader.readLine()) != null) {
                sb.append(inline);
            }
            InputSource is = new InputSource(new StringReader(sb.toString()));
            Document doc = builder.parse(is);
            doc.getDocumentElement()
                    .normalize();
            NodeList items = doc.getElementsByTagName("item");
            for (int i = 0; i < items.getLength(); i++) {
                Node item = items.item(i);
                if (item.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) item;
                    SearchResultItem resultItem = new SearchResultItem();
                    resultItem.setTitle(element.getElementsByTagName("title")
                                                .item(0)
                                                .getTextContent());
                    resultItem.setLink(element.getElementsByTagName("link")
                                               .item(0)
                                               .getTextContent());
                    resultItem.setDescription(element.getElementsByTagName("description")
                                                      .item(0)
                                                      .getTextContent());
                    resultItem.setPubDate(element.getElementsByTagName("pubDate")
                                                  .item(0)
                                                  .getTextContent());
                    resultItems.add(resultItem);
                }
                if (i == 5) {
                    break;
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}
