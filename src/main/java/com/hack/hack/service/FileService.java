package com.hack.hack.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.hack.hack.azure.AzureBlobAdapter;
import com.hack.hack.dto.AcknowledgementDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hack.hack.entity.Acknowledgement;
import com.hack.hack.entity.Compliance;
import com.hack.hack.entity.Form;
 
import com.hack.hack.entity.FormTemplate;
import com.hack.hack.repository.AcknowledgementRepository;
 
import com.hack.hack.repository.ComplianceRepository;
import com.hack.hack.repository.FormRepository;
import com.hack.hack.repository.FormTemplateRepository;
@Service
public class FileService {
	
	  @Autowired
	  public ComplianceRepository complianceRepository;
	  
	  @Autowired
	  public FormRepository formRepository;

	  @Autowired
	  public AzureBlobAdapter azureBlobAdapter;

	 
	 @Autowired
	 public FormTemplateRepository formTemplateRepository;
	 
	 @Autowired
	 public AcknowledgementRepository ackRepository;

	public void uploadComplianceTransaction( MultipartFile files, int complianceId, int fileId, int templateId) {
		Compliance compl = complianceRepository.findById(complianceId).get();
		//FormTemplate template =formTemplateRepository.findById(fileId).get();
		String path= azureBlobAdapter.upload(files, "form");
		Optional<Form> form = formRepository.findById(fileId);
		FormTemplate template =  formTemplateRepository.findById(templateId).get();
		if(form.isPresent()) {
			Form existingForm =form.get();
			existingForm.setFormName(template.getFormName());
			existingForm.setCompliance(compl);
			existingForm.setFormTemplate(template);
			existingForm.setPath(path);
			formRepository.save(existingForm);
		}else {
		
		if(compl.getForms().isEmpty()) {
			
			Form newForm = new Form();
			newForm.setFormName(template.getFormName());
			newForm.setFormTemplate(template);
			newForm.setCompliance(compl);
			newForm.setPath(path);
			formRepository.save(newForm);
		}
		 
		}
		
	
	    /* File convFile = new File("/Users/sansur/work/hackathon/transaction_forms/"+files.getOriginalFilename());
	     try {
			files.transferTo(convFile);
		} catch (Exception e) {
			 
		}*/
	         
    }
	public void uploadChallan( MultipartFile files, int complianceId) {
		Compliance compl = complianceRepository.findById(complianceId).get();
	 
	     Acknowledgement ack = new Acknowledgement();
	    String path = azureBlobAdapter.upload(files, "template");
	     ack.setPath(path);
	     ack.setCompliance(compl);
	     ack.setName(files.getOriginalFilename());
	     ackRepository.save(ack);
	      
			/*
			 * File convFile = new
			 * File("/Users/sansur/work/hackathon/acknowledgement/"+files.
			 * getOriginalFilename()); try { files.transferTo(convFile); } catch (Exception
			 * e) {
			 * 
			 * }
			 */  
    }
	public List<Acknowledgement> getChallan( int complianceId) {
		List<Acknowledgement> list = ackRepository.findByComplianceId(complianceId);
		return list;
	}
	
	 

    public String uploadFormTemplateFile(MultipartFile files) {
		return azureBlobAdapter.upload(files, "template");
	}

	public byte[] getFile(String fileName) {
		return azureBlobAdapter.getFile(fileName);
	}

	public boolean delete(String fileName) {
		return azureBlobAdapter.deleteFile(fileName);
	}
	public  File getFile(int id, boolean isTemplate) {
		String path = null;
		if(isTemplate) {
			FormTemplate template =  formTemplateRepository.findById(id).get();
			 path = template.getPath();
		}
		else {
			Form form =  formRepository.findById(id).get();
			path = form.getPath();
		}
		File file = new File (path);
		// TODO Auto-generated method stub
		return file;
	}
	public List<AcknowledgementDTO> getChallanForYearMonth(int year, int month) {
		 
		List<Acknowledgement> list = ackRepository.getAcknowledgementsForMonthAndYear(month, year);
		List<AcknowledgementDTO> dtoList = new ArrayList<>();
		for(Acknowledgement ack:list) {
			AcknowledgementDTO dto = new AcknowledgementDTO();
			dto.setFileName(ack.getName());
			dto.setPath(ack.getPath());
			dto.setComplianceName(ack.getCompliance().getName());
			dtoList.add(dto);
		}
		return dtoList;
	}

}
