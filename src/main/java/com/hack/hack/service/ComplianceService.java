package com.hack.hack.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.hack.hack.constant.Status;
import com.hack.hack.dto.ComplianceDTO;
import com.hack.hack.dto.ComplianceRequest;
import com.hack.hack.dto.FormsDTO;
import com.hack.hack.entity.BulkImport;
import com.hack.hack.entity.Compliance;
import com.hack.hack.entity.ComplianceTemplate;
import com.hack.hack.entity.Form;
import com.hack.hack.entity.FormTemplate;
import com.hack.hack.entity.User;
import com.hack.hack.repository.ComplianceRepository;
import com.hack.hack.repository.ComplianceTemplateRepository;
import com.hack.hack.repository.FormRepository;
import com.hack.hack.repository.FormTemplateRepository;
import com.hack.hack.repository.StatusRepository;
import com.hack.hack.repository.UserRepository;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

@Service
public class ComplianceService {

    @Autowired
    public ComplianceRepository complianceRepository;

    @Autowired
    public StatusRepository statusRepository;

    @Autowired
    public UserRepository userRepository;
    @Autowired
    public FormRepository formRepository;

    @Autowired
    public ComplianceTemplateRepository complianceTemplateRepository;

    @Autowired
    public FormTemplateRepository formTemplateRepository;


	 String sendGridApi="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";
	

    public void save(ComplianceRequest complianceReq) {
    	Compliance compliance = complianceRepository.findById(complianceReq.getId()).get();
    	  compliance.setUser(complianceReq.getUserId());
    	  compliance.setStatus(statusRepository.findById(complianceReq.getStatusId()).get());
    	  compliance.setDueDate(complianceReq.getDueDate());
    	  compliance.setComments(complianceReq.getComment());
    	  
    	 User user = userRepository.findById(complianceReq.getUserId()).get();
        complianceRepository.save(compliance);
        String text = getText(user,compliance);
        String subject ="New updates available for Compliance - "+compliance.getName();
       
      sendEmail(text,subject);
    }
  
	private String getText(User user, Compliance compliance) {
		
		String template = "<p>Dear Team,</p>"+
				 "<p>There are new updates available for the compliance : "+compliance.getName() +" </p>"
		       + "<table>"+
		"<tr>"+
		    "<td>Assignee</td>"+
		   "<td>"+user.getName()+"</td>"+
		   "</tr>"+
		   "<tr>"+
		   "<td>Email</td>"+
		   "<td>"+user.getEmail()+"</td>"+
		   "</tr>"+
		   "<tr>"+
		   "<td>Due Date</td>"+
		   "<td>"+compliance.getDueDate()+"</td>"+
		   "</tr>"+
		   "<tr>"+
		   "<td>Status</td>"+
		   "<td>"+compliance.getStatus().getName()+"</td>"+
		   "</tr>"+
		   "</table>"+
		   "<p>Please visit <a href=\"http://local.meetx.com:8080/ssoauth/ssologin/\">Compliance Portal</a> for more details</p>"+
		   "<p>Thanks & Regards,</p> "+
		   "<p>Corporate Compliance Team</p>";
		return template;
	}

	void sendEmail(String text, String subject) {
	  sendEmail("sangeetha.surendran@nasdaq.com", subject,new Content("text/html", text));
	}
    public void bulkUpload(List<BulkImport> rows) {
        List<Compliance> complianceList = new ArrayList<>();
        for (BulkImport bulk: rows) {
            final Optional<ComplianceTemplate> byComplianceName = Optional.ofNullable(complianceTemplateRepository.findByComplianceName(bulk.getTitle()));
            Compliance compliance = new Compliance();
            byComplianceName.orElseThrow(() -> new ServiceException("Compliance Template"));
            compliance.setComplianceFormTemplate(byComplianceName.get().getId());
            compliance.setUser(1);
            compliance.setDueDate(bulk.getDueDate());
            compliance.setDescription(bulk.getDescription());
            compliance.setName(bulk.getTitle());
            compliance.setStatus(statusRepository.findById(Status.NEW.getStatus()).get());
            compliance.setCreatedDate(new Date());
            compliance.setUpdatedDate(new Date());
            complianceList.add(compliance);
        }
        String text = getTextForUpload();
        
         String subject ="New updates available in compliance calendar!";
        sendEmail( text, subject);
        complianceRepository.saveAll(complianceList);
    }

	private String getTextForUpload() {
		String text = "<p>Dear Team,</p>"
	        		+ "<p>There are new updates available in compliance calendar.</p>"
	        		+"<p>Please visit <a href=\"http://local.meetx.com:8080/ssoauth/ssologin/\">Compliance Portal</a> for more details</p>"
 					+ "<p>Thanks & Regards,</p><p>Corporate Compliance Team</p>";
		return text;
	}

	public ComplianceDTO getCompliance(int complianceId) {
		ComplianceDTO complianceDTO = new ComplianceDTO();

		Optional<Compliance> compliance = complianceRepository.findById(complianceId);
		Compliance compl = compliance.get();
		complianceDTO.setDueDate(compl.getDueDate());
		complianceDTO.setComments(compl.getComments());
		complianceDTO.setName(compl.getName());
		complianceDTO.setDescription(compl.getDescription());
		complianceDTO.setId(compl.getId());
		final Optional<User> byId = userRepository.findById(compl.getUser());
		if(byId.isPresent()){
			complianceDTO.setAssignee(byId.get());
		}
		//To be added
		List<FormsDTO> formsDTOList = new ArrayList<>();
		List<Form> formList =formRepository.findByComplianceId(complianceId);
		ComplianceTemplate compTemplate = complianceTemplateRepository.findById(compl.getComplianceFormTemplate()).get();
		List<FormTemplate> formTemplateList = formTemplateRepository.findByComplianceTemplate(compTemplate);
		
		if (!formTemplateList.isEmpty()) {
			for (FormTemplate template : formTemplateList) {
				FormsDTO formDTO = new FormsDTO();
				

					formDTO.setName(template.getFormName());
					formDTO.setTemplatePath(template.getPath());
					formDTO.setTemplateId(template.getId());
					for (Form form : formList) {
						if (form.getFormTemplate().getId() == template.getId()) {
							complianceDTO.setFileUploaded(true);
							formDTO.setFormId(form.getId());
							formDTO.setFormPath(form.getPath());
						}
					}
					formsDTOList.add(formDTO);
				 
			}
		}
		 
		complianceDTO.setStatus( compl.getStatus());
		complianceDTO.setForms(formsDTOList);
		return complianceDTO;
		 
	}

	public List<com.hack.hack.entity.Status> getStatusList() {
		Iterable<com.hack.hack.entity.Status> iterable =  statusRepository.findAll();
		 List<com.hack.hack.entity.Status> result =  StreamSupport.stream(iterable.spliterator(), false)
				    .collect(Collectors.toList());
		return result;
	}

	public List<Compliance> getAllCompliances() {
        return complianceRepository.findAll();
    }

    public  List<Compliance> getAllCompliancesForCurrentMonth(){
        return complianceRepository.getComplianceForAMonthAndAYear(new Date().getMonth() + 1, new Date().getYear() + 1900);
    }

	public List<User> getUserList() {
		Iterable<User> iterable =  userRepository.findAll();
		 List<User> result =  StreamSupport.stream(iterable.spliterator(), false)
				    .collect(Collectors.toList());
		return result;
	}
	
	public void saveCompliance(Compliance compliance) {
		  complianceRepository.save(compliance);
		  
	}
	public List<FormTemplate> getFormTemplatesByComplianceID(int complianceId) {
		List<FormTemplate> formTemplates = formTemplateRepository.findByComplianceTemplateId(complianceId);
		return formTemplates;
	}
	
	 
	public Response sendEmail( String to, String subject, Content content) {
		 SendGrid sg = new SendGrid(sendGridApi);
		Mail mail = new Mail(new Email("nasdaq.hack.comp.app@outlook.com"), subject, new Email(to), content);
		mail.setReplyTo(new Email("nasdaq.hack.comp.app@outlook.com"));
		Request request = new Request();
		
		Response response = null;
		try {
		request.setMethod(Method.POST);
		request.setEndpoint("mail/send");
		
		request.setBody(mail.build());
		response = sg.api(request);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
		
	}

}
