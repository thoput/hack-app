package com.hack.hack.service;

import com.hack.hack.entity.ComplianceTemplate;
import com.hack.hack.repository.ComplianceTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ComplianceTemplateService {

    @Autowired
    public ComplianceTemplateRepository complianceTemplateRepository;

    public ComplianceTemplate save(String complianceName, String category) {
        ComplianceTemplate complianceTemplate = new ComplianceTemplate();
        complianceTemplate.setComplianceName(complianceName);
        complianceTemplate.setCreatedDate(new Date());
        complianceTemplate.setUpdatedDate(new Date());
        complianceTemplate.setCategory(category);
        complianceTemplate = complianceTemplateRepository.save(complianceTemplate);
        return complianceTemplate;
    }

    public Optional<ComplianceTemplate> findById(int id) {
        return complianceTemplateRepository.findById(id);
    }

    public List<ComplianceTemplate> findAll() {
        return (List<ComplianceTemplate>) complianceTemplateRepository.findAll();
    }
}
