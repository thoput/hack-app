package com.hack.hack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hack.hack.entity.Acknowledgement;

public interface AcknowledgementRepository extends JpaRepository<Acknowledgement, Integer>  {

	List<Acknowledgement> findByComplianceId(int complianceId);
	
    @Query(value = "select * from acknowledgement a, compliance c where a.compliance_id=c.id and MONTH(c.due_date) = :month and YEAR(c.due_date) = :year", nativeQuery = true)
    List<Acknowledgement> getAcknowledgementsForMonthAndYear(@Param("month") Integer month,@Param("year") Integer year);


}
