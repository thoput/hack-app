package com.hack.hack.repository;

import org.springframework.data.repository.CrudRepository;

 
import com.hack.hack.entity.Status;

public interface StatusRepository extends CrudRepository<Status, Integer>  {

}
