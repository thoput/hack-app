package com.hack.hack.repository;

import com.hack.hack.entity.ComplianceTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplianceTemplateRepository extends CrudRepository<ComplianceTemplate, Integer> {
    ComplianceTemplate findByComplianceName(String complianceName);
}
