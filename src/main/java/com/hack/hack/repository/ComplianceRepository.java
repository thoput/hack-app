package com.hack.hack.repository;

import com.hack.hack.entity.Compliance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplianceRepository extends JpaRepository<Compliance, Integer> {

    @Query(value = "select * from compliance where MONTH(due_date) = :month and YEAR(due_date) = :year", nativeQuery = true)
    List<Compliance> getComplianceForAMonthAndAYear(@Param("month") Integer month,@Param("year") Integer year);

}
