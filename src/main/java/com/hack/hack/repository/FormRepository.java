package com.hack.hack.repository;

import com.hack.hack.entity.Form;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormRepository extends CrudRepository<Form, Integer> {

	List<Form> findByComplianceId(int complianceId);
}
