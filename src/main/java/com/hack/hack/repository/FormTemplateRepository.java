package com.hack.hack.repository;

import com.hack.hack.entity.ComplianceTemplate;
import com.hack.hack.entity.FormTemplate;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormTemplateRepository extends CrudRepository<FormTemplate, Integer> {
	List<FormTemplate> findByComplianceTemplateId(int complianceTemplateId);

	List<FormTemplate> findByComplianceTemplate(ComplianceTemplate compTemplate);
}
