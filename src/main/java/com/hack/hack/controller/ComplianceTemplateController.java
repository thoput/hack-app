package com.hack.hack.controller;

import com.hack.hack.entity.ComplianceTemplate;
import com.hack.hack.entity.FormTemplate;
import com.hack.hack.service.ComplianceService;
import com.hack.hack.service.ComplianceTemplateService;
import com.hack.hack.service.FileService;
import com.hack.hack.service.FormTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;
import java.util.List;

@RestController
public class ComplianceTemplateController {

    @Autowired
    private ComplianceTemplateService complianceTemplateService;

    @Autowired
    private FormTemplateService formTemplateService;

    @Autowired
    private FileService fileService;

    @PostMapping("/complianceTemplate")
    public Integer save(@RequestBody ComplianceRequestData complianceRequestData) {
        final ComplianceTemplate save = complianceTemplateService.save(complianceRequestData.complianceName, complianceRequestData.category);
        return save.getId();
    }

    @GetMapping("/complianceTemplate")
    public List<ComplianceTemplate> get() {
        return complianceTemplateService.findAll();
    }

    @PostMapping("saveFormTemplateName")
    public Integer saveFormTemplate(@RequestBody FormTemplateRequestData data) {
        final FormTemplate formTemplate = formTemplateService.saveName(data.formName, data.complianceId);
        return formTemplate.getId();
    }

    @DeleteMapping("formTemplate")
    public String deleteFormTemplate(@RequestParam("formId") Integer formId) {
        final String fileName = formTemplateService.delete(formId);
        fileService.delete(fileName);
        return "SUCCESS";
    }

    @GetMapping("file")
    public byte[] file(@RequestParam("fileName") String fileName) {
        return fileService.getFile(fileName);
    }
}

class FormTemplateRequestData {
    public String formName;
    public int complianceId;
}

class ComplianceRequestData {
    public String complianceName;
    public String category;
}
