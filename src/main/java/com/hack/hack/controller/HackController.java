package com.hack.hack.controller;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
import com.hack.hack.service.FormTemplateService;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hack.hack.dto.AcknowledgementDTO;
import com.hack.hack.dto.ComplianceDTO;
import com.hack.hack.dto.ComplianceRequest;
import com.hack.hack.entity.Acknowledgement;
import com.hack.hack.entity.BulkImport;
import com.hack.hack.entity.Compliance;
import com.hack.hack.entity.FormTemplate;
import com.hack.hack.entity.SearchResultItem;
import com.hack.hack.entity.Status;
import com.hack.hack.entity.User;
import com.hack.hack.service.ComplianceService;
import com.hack.hack.service.FileService;
import com.hack.hack.service.SearchService;
import com.hack.hack.util.FileUtil;

@Controller

public class HackController {

    @Autowired
    private SearchService searchService;

    @Autowired
    private ComplianceService complianceService;

    @Autowired
    private FileService fileService;

    @Autowired
    private FormTemplateService formTemplateService;


    @RequestMapping(value = "news", method = RequestMethod.GET)
    public @ResponseBody List<SearchResultItem> news () {
        String tag = "Corporate compliance calendar india 2020";
        return searchService.search(tag);
    }

    @RequestMapping(value = "bulkupload", method = RequestMethod.POST)
    public ResponseEntity<String> importExcelFile(@RequestParam("file") MultipartFile files) throws IOException {
        List<BulkImport> rows = new ArrayList<>();

        FileUtil.extractData(files, rows);

        complianceService.bulkUpload(rows);

        return ResponseEntity.status(HttpStatus.OK).body("SUCCESS");
    }
    @RequestMapping(value = "compliance", method = RequestMethod.GET)
    public ResponseEntity<ComplianceDTO> compliance(@RequestParam("id") int complianceId) throws IOException {
         ComplianceDTO compliance = complianceService.getCompliance(complianceId);
        return ResponseEntity.status(HttpStatus.OK).body(compliance);
    }

    @RequestMapping(value = "statuses", method = RequestMethod.GET)
    public ResponseEntity<List<Status>>  statuses() throws IOException {
         List<Status> statusList = complianceService.getStatusList();
        return ResponseEntity.status(HttpStatus.OK).body(statusList);
    }
    @RequestMapping(value = "users", method = RequestMethod.GET)
    public ResponseEntity<List<User>>  users() throws IOException {
         List<User> userList = complianceService.getUserList();
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }
    @RequestMapping(value = "compliances", method = RequestMethod.GET)
    public ResponseEntity<List<Compliance>> compliance() {
        List<Compliance> compliances = complianceService.getAllCompliances();
        return ResponseEntity.status(HttpStatus.OK).body(compliances);
    }

    @RequestMapping(value = "/current/compliances", method = RequestMethod.GET)
    public ResponseEntity<List<Compliance>> currentCompliance() {
        List<Compliance> compliances = complianceService.getAllCompliancesForCurrentMonth();
        return ResponseEntity.status(HttpStatus.OK).body(compliances);
    }
 
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public  ResponseEntity<String> upload(@RequestParam("file") MultipartFile files,@RequestParam("transaction") boolean transaction,@RequestParam("complianceId") int complianceId,@RequestParam("fileId") int fileId,@RequestParam("templateId")  int templateId) throws IOException {
      
    	if(transaction) {
 
    	fileService.uploadComplianceTransaction(files,complianceId,fileId,templateId);
    	}
       else {
             final String s = fileService.uploadFormTemplateFile(files);
             if (null != s) {
                 formTemplateService.savePath(fileId, s);
             }
         }
        return ResponseEntity.status(HttpStatus.OK).body("SUCCESS"); 
      
    }
     @RequestMapping(value = "/uploadChallan", method = RequestMethod.POST)
    public  ResponseEntity<String> uploadChallans(@RequestParam("file") MultipartFile files,@RequestParam("complianceId") int complianceId) throws IOException {
    	fileService.uploadChallan(files,complianceId);
        return ResponseEntity.status(HttpStatus.OK).body("SUCCESS"); 
      
    } 
     
     @RequestMapping(value = "/challans", method = RequestMethod.GET)
     public    ResponseEntity<List<Acknowledgement>> challans(@RequestParam("complianceId") int complianceId) throws IOException {
    	 List<Acknowledgement> acks = fileService.getChallan(complianceId);
         return ResponseEntity.status(HttpStatus.OK).body(acks);
       
     } 
     
     
     @RequestMapping(value = "/formTemplates", method = RequestMethod.GET)
     public    ResponseEntity<List<FormTemplate>> formTemplates(@RequestParam("complianceId") int complianceId) throws IOException {
    	 List<FormTemplate> templates = complianceService.getFormTemplatesByComplianceID(complianceId);
         return ResponseEntity.status(HttpStatus.OK).body(templates);
       
     } 
     
 
    /*@RequestMapping(value = "/uploadChallans", method = RequestMethod.POST)
    public  ResponseEntity<String> challans(@RequestParam("file") MultipartFile files) throws IOException {
    	fileService.upload(files);
        return ResponseEntity.status(HttpStatus.OK).body("SUCCESS");

    }*/
 
    
    @RequestMapping(value = "/saveCompliance", method = RequestMethod.POST)
    public  ResponseEntity<ComplianceDTO> saveCompliance(@RequestBody ComplianceRequest compliance ) throws IOException {
        complianceService.save(compliance);
        ComplianceDTO complianceDTO = complianceService.getCompliance(compliance.getId());
        return ResponseEntity.status(HttpStatus.OK).body(complianceDTO); 
      
    }
    
    @RequestMapping(path = "/download", method = RequestMethod.GET)
    public ResponseEntity<String> download(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") int id, @RequestParam("isTemplate") boolean isTemplate ) throws IOException {
    	File file =fileService.getFile(id ,isTemplate);
    	 
    	 
		if (file.exists()) {

			//get the mimetype
			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				//unknown mimetype so set the mimetype to application/octet-stream
				mimeType = "application/octet-stream";
			}

			response.setContentType(mimeType);
 
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

			 //Here we have mentioned it to show as attachment
			 //response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

			response.setContentLength((int) file.length());

			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

			FileCopyUtils.copy(inputStream, response.getOutputStream());

		}
        return   ResponseEntity.status(HttpStatus.OK).body("SUCCESS"); 
    }
    

    @RequestMapping(value = "/challanForYearMonth", method = RequestMethod.GET)
    public    ResponseEntity<List<AcknowledgementDTO>> challansForYearMonth(@RequestParam("year") int year,@RequestParam("month") int month) throws IOException {
   	 List<AcknowledgementDTO> acks = fileService.getChallanForYearMonth(year,month);
        return ResponseEntity.status(HttpStatus.OK).body(acks);
      
    } 
   
}
