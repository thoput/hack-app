package com.hack.hack.controller;

import com.hack.hack.entity.Compliance;
import com.hack.hack.entity.User;
import com.hack.hack.repository.UserRepository;
import com.hack.hack.service.ComplianceService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ComplianceController {

    @Autowired
    private ComplianceService complianceService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("table/compliance")
    public List<ComplianceDTO> getComplianceWithAssignee() {
        List<ComplianceDTO> complianceDTOs = new ArrayList<ComplianceDTO>();
        List<Compliance> compliances = complianceService.getAllCompliancesForCurrentMonth();
        for (Compliance compliance: compliances) {
            ComplianceDTO complianceDTO = new ComplianceDTO();
            complianceDTO.compliance = compliance;
            final Optional<User> byId = userRepository.findById(compliance.getUser());
            byId.orElseThrow(() -> new ServiceException("User not present"));
            complianceDTO.user = byId.get();
            complianceDTOs.add(complianceDTO);
        }
        return  complianceDTOs;
    }
}

class ComplianceDTO {
    public User user;
    public Compliance compliance;
}
