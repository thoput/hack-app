package com.hack.hack.controller;

import com.hack.hack.entity.Compliance;
import com.hack.hack.entity.Notification;
import com.hack.hack.entity.User;
import com.hack.hack.repository.ComplianceRepository;
import com.hack.hack.repository.NotificationRepository;
import com.hack.hack.repository.UserRepository;
import com.hack.hack.service.ComplianceService;
import com.hack.hack.service.NotificationService;
import com.sendgrid.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ComplianceService complianceService;

    @Autowired
    private ComplianceRepository complianceRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private UserRepository userRepository;

    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @PostMapping("notification")
    public Notification save(@RequestBody NotificationRequestData notificationRequestData) {
        Notification notification = new Notification();
        notification.setTitle(notificationRequestData.title);
        notification.setSource(notificationRequestData.source);
        notification.setPubDate(notificationRequestData.pubDate);
        notification.setCreatedDate(new Date());
        notification.setUpdatedDate(new Date());

        notificationService.save(notification);
        return notification;
    }

    @GetMapping("notification")
    public List<Notification> getNotifications() {
        return notificationService.getNotifications();
    }


    @PostConstruct
    public void setUp() {
        executorService.scheduleAtFixedRate(() -> methodCallAtFixedInterval(), 0, 5, TimeUnit.MINUTES);
    }

    private void methodCallAtFixedInterval() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        final List<Compliance> all = complianceRepository.findAll();
        final List<Notification> byPubDate = notificationRepository.getNotification(calendar.getTime());
        if (byPubDate.size() == 0) {
            for (Compliance comp : all) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(comp.getDueDate());
                cal.add(Calendar.DATE, -2);
                Date dateBefore = cal.getTime();
                if (dateBefore.compareTo(calendar.getTime()) == 0) {
                    NotificationRequestData notificationRequestData = new NotificationRequestData();
                    notificationRequestData.title = comp.getName();
                    notificationRequestData.source = "System";
                    notificationRequestData.pubDate = comp.getDueDate();
                    save(notificationRequestData);
                    Optional<User> user = userRepository.findById(comp.getId());
                    String body = "Alert : Due date for " + comp.getName() + " is 2 days to go";
                    Content content =  new Content();
                    content.setType("Text");
                    content.setValue(body);
                    complianceService.sendEmail(user.get().getEmail(),"Compliance Due Date Alert "+comp.getName(), content);
                }
            }
        }

    }
}

class NotificationRequestData {
    public String title;
    public String source;
    public Date pubDate;
}