package com.hack.hack.util;

import com.hack.hack.entity.BulkImport;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class FileUtil {
    public static void extractData(MultipartFile files, List<BulkImport> rows) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                BulkImport rowObj = new BulkImport();

                XSSFRow row = worksheet.getRow(index);
                rowObj.setCategory(row.getCell(0).getStringCellValue());
                rowObj.setTitle(row.getCell(1).getStringCellValue());
                rowObj.setDescription(row.getCell(2).getStringCellValue());
                rowObj.setDueDate(row.getCell(3).getDateCellValue());
                rows.add(rowObj);
            }
        }
    }
}
