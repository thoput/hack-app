package com.hack.hack.dto;

import java.util.Date;
import java.util.List;

import com.hack.hack.entity.Status;
import com.hack.hack.entity.User;

public class ComplianceDTO {
	private int id;
	private User assignee;
	private Status status;
	private Date dueDate;
	private String comments;
	private String name;
	private String description;
	private boolean fileUploaded;
	
	 public boolean isFileUploaded() {
		return fileUploaded;
	}
	public void setFileUploaded(boolean fileUploaded) {
		this.fileUploaded = fileUploaded;
	}
	private List<FormsDTO> forms;
	 
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public User getAssignee() {
		return assignee;
	}
	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<FormsDTO> getForms() {
		return forms;
	}
	public void setForms(List<FormsDTO> forms) {
		this.forms = forms;
	}
	
}
