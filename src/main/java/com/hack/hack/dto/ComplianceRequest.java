package com.hack.hack.dto;

import java.util.Date;

public class ComplianceRequest {
//  const compliance = {id:  this.compliance.id, dueDate: this.changedDate, status:this.statusId, userId:this.assigneeId,comment:''}
    
	int id;
	Date dueDate;
	int statusId;
	int userId;
	String comment;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
}
